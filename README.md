# LODAtlas Summary Generator
This project is a wrapper for the [RDF summary generation API](https://gitlab.inria.fr/scebiric/rdfsummary/tree/master) to compute RDF summary for a given RDF file in N-Triples format.

The program takes the absolute path to the RDF file in N-Triples format to process as input. It expects to find an ontology file in Turtle format that contains the vocabularies used inside the RDF file to process.

It generates the summary file in N-Triples format as output in the same directory with the RDF file.

# Usage
```
java -jar lodatlas-summarygenerator-prod.jar <path to RDF file>
```

To be able to compile the project, the modifications to `settings.xml` file should be applied as indicated in the **Set up** and **RDFSummary jar** sections of the [README.md](https://gitlab.inria.fr/scebiric/rdfsummary/blob/master/README.md) file of the RDF summary repository. 

# Dependencies
* PostgreSQL 9.4 and up
* Java 1.8

# License
LODAtlas Summary Generator is licensed under [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)