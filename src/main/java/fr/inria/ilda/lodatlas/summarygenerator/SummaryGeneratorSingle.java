/*******************************************************************************
 * LODSummaryGenerator
 * Copyright (C) 2018  INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.summarygenerator;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.oak.RDFSummary.Summarizer;
import fr.inria.oak.RDFSummary.config.ParametersException;
import fr.inria.oak.RDFSummary.config.params.ConnectionParams;
import fr.inria.oak.RDFSummary.config.params.ExportParams;
import fr.inria.oak.RDFSummary.config.params.SummarizationParams;
import fr.inria.oak.RDFSummary.config.params.SummarizerParams;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DdlDaoImpl;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDaoImpl;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDaoImpl;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDaoImpl;
import fr.inria.oak.RDFSummary.data.handler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.handler.SqlConnectionHandlerImpl;
import fr.inria.oak.RDFSummary.data.loader.JenaException;
import fr.inria.oak.RDFSummary.data.loader.PsqlCopyDataLoader;
import fr.inria.oak.RDFSummary.data.loader.PsqlDataLoader;
import fr.inria.oak.RDFSummary.data.queries.DdlQueriesImpl;
import fr.inria.oak.RDFSummary.data.queries.DictionaryQueriesImpl;
import fr.inria.oak.RDFSummary.data.queries.DmlQueriesImpl;
import fr.inria.oak.RDFSummary.data.queries.RdfTablesQueriesImpl;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.timer.TimerImpl;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.PostgresDataSource;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * This is a wrapper class to run RDF summaries software hosted at
 * https://gitlab.inria.fr/scebiric/rdfsummary/tree/master. <br>
 * 
 * This program should be run as an external jar as the RDF summary code uses an
 * API that exits upon receiving exception.
 * 
 * @author Hande Gözükan
 *
 */
public class SummaryGeneratorSingle {

	private static final Logger logger = LoggerFactory.getLogger(SummaryGeneratorSingle.class.getName());

	private final static String DATASOURCE_NAME = "postgres";

	private final static String HOST = "localhost";

	private final static int PORT = 5234;

	private final static String DATABASE_NAME = "postgres";

	private final static String USER_NAME = "postgres";

	private final static String PASSWORD = "postgres";

	private final static String SUMMARY_TYPE = "weak";

	private final static String DATA_LOADER = "copy";

	private final static String SCHEMA_NAME = "schema";

	/**
	 * To run RDF summaries, the program requires the absolute path to the RDF file
	 * in N-Triples format. <br>
	 * 
	 * The program expects to find a concatenated vocabulary file which has the same
	 * file path with the RDF file with an extension "_vocab.ttl" at the end.<br>
	 * 
	 * The output RDF summary file has the same file path with RDF file to process
	 * with an extension "_rdfsum.nt" at the end.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length > 1) {

			String ntFilePath = args[0];
			File ntFile = new File(ntFilePath);
			String resourceDirectory = ntFile.getParent();

			logger.info("");
			logger.info("");
			logger.info("Starting RDF summary for " + ntFilePath);
			logger.info("");
			logger.info("");
			ConnectionParams connectionParams = new ConnectionParams(DATASOURCE_NAME, HOST, PORT, DATABASE_NAME,
					USER_NAME, PASSWORD);

			Summarizer summarizer = new Summarizer();

			SummarizationParams summarizationParams = new SummarizationParams(SUMMARY_TYPE, SCHEMA_NAME, ntFilePath,
					DATA_LOADER, "./");
			summarizationParams.setDropSchema(false);
			summarizationParams.setSaturateInput(false);
			summarizationParams.setFetchSize(100000);

			// set vocabulary file if it
			// exists
			String vocabularyFilePath = ntFilePath + "_vocab.ttl";
			File vocabFile = new File(vocabularyFilePath);
			logger.debug("Vocabulary file " + vocabularyFilePath);

			if (vocabFile.exists()) {
				summarizationParams.setSchemaTriplesFilepath(vocabularyFilePath);
			}
			ExportParams exportParams = new ExportParams(resourceDirectory);
			exportParams.setExportToRdfFile(true);
			exportParams.setExportRepresentationTables(false);

			SummarizerParams summarizerParams = new SummarizerParams(connectionParams, summarizationParams,
					exportParams);

			SqlConnectionHandler handler = null;

			try {
				PostgresDataSource datasource = new PostgresDataSource(HOST, PORT, DATABASE_NAME, USER_NAME, PASSWORD);
				handler = new SqlConnectionHandlerImpl(datasource.getConnection());
				// Drop schema command format:
				// DROP SCHEMA [IF EXISTS] schema_name [CASCADE | ..]
				// CASCADE:
				// Automatically drop objects (tables, functions, etc.) that are contained in
				// the schema, and in turn all objects that depend on those object
				handler.prepareStatement("DROP SCHEMA IF EXISTS schema CASCADE").execute();

				DdlDao ddldao = new DdlDaoImpl(handler, 100000, new TimerImpl(), new DdlQueriesImpl());
				DmlDao dmlDao = new DmlDaoImpl(handler, 100000, new TimerImpl(), new DmlQueriesImpl());
				DictionaryDao dictDao = new DictionaryDaoImpl("g_dict", handler, 100000, new DictionaryQueriesImpl(),
						new TimerImpl());
				RdfTablesDao rdfTablesDao = new RdfTablesDaoImpl(handler, 100000, new RdfTablesQueriesImpl(), dictDao,
						ddldao, dmlDao, new TimerImpl());

				PsqlDataLoader dataLoader = new PsqlCopyDataLoader(ddldao, dmlDao, rdfTablesDao, new TimerImpl());
				dataLoader.loadDataset(ntFilePath, SCHEMA_NAME, "schema.g");
				handler.prepareStatement("UPDATE schema.g SET o = left(o, 250)").executeUpdate();

			} catch (JenaException e) {
				logger.error("Received JenaException trying to load dataset", e);
				System.exit(1);
			} catch (SQLException e) {
				logger.error("Received SQLException trying to load dataset", e);
				System.exit(1);
			} catch (IOException e) {
				logger.error("Received IOException trying to load dataset", e);
				System.exit(1);
			}

			try {
				summarizer.summarize(summarizerParams);
				Summarizer.closeApplicationContext();
				handler.prepareStatement("DROP SCHEMA IF EXISTS schema CASCADE").execute();

				// Vacuum command should clean the tables on the disk and leave free space to
				// system
				// If you do not run this, even if you drop the schemas, they do not free disk
				// space, they are still stored on the disk.
				logger.debug("Running vacuum " + System.currentTimeMillis());
				handler.prepareStatement("VACUUM FULL").execute();
				logger.debug("Finished running vacuum " + System.currentTimeMillis());

				File rdf = new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt");
				if (rdf.exists()) {
					rdf.renameTo(new File(ntFilePath + "_rdfsum.nt"));
					logger.debug("Changed file name to " + ntFilePath + "_rdfsum.nt");

				}

			} catch (ConfigurationException e) {
				logger.error("Received ConfigurationException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (PSQLException e) {
				logger.error("Received PSQLException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (SQLException e) {
				logger.error("Received SQLException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (JenaException e) {
				logger.error("Received JenaException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (PsqlStorageException e) {
				logger.error("Received PsqlStorageException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (SchemaException e) {
				logger.error("Received SchemaException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (UnsupportedDatabaseEngineException e) {
				logger.error("Received UnsupportedDatabaseEngineException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (DictionaryException e) {
				logger.error("Received DictionaryException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (InexistentValueException e) {
				logger.error("Received InexistentValueException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (InexistentKeyException e) {
				logger.error("Received InexistentKeyException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (IOException e) {
				logger.error("Received IOException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (ParametersException e) {
				logger.error("Received ParametersException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (QueryException e) {
				logger.error("Received QueryException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (UnsupportedStorageSchemaException e) {
				logger.error("Received UnsupportedStorageSchemaException trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			} catch (Exception e) {
				logger.error("Received Exception trying to generate RDF summary", e);
				clear(new File(resourceDirectory + "/" + SCHEMA_NAME + "_g_enc_weak.nt"));
				System.exit(1);
			}

		}
	}

	private static void clear(File rdf) {
		if (rdf.exists()) {
			rdf.delete();
			logger.debug("Deleted rdfsum file because of exception");

		}
	}
}
